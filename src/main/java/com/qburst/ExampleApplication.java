package com.qburst;

import com.qburst.resource.ExampleResource;
import com.qburst.dao.MyDao;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.Application;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.views.ViewBundle;
import org.eclipse.jetty.server.session.SessionHandler;
import org.skife.jdbi.v2.DBI;

import java.util.Map;


/**
 * Created by nithin on 23/4/15.
 */
public class ExampleApplication extends Application<ExampleConfiguration>{
    public static void main(String[] args) throws Exception {
        new ExampleApplication().run(args);
    }
    public void initialize(Bootstrap<ExampleConfiguration> bootstrap) {
        bootstrap.addBundle(new ViewBundle<ExampleConfiguration>() {
            @Override
            public Map<String, Map<String, String>> getViewConfiguration(ExampleConfiguration config) {
                return config.getViewRendererConfiguration();
            }
        });
    }

    @Override
    public void run(ExampleConfiguration config, Environment environment) {
      final DBIFactory factory = new DBIFactory();
      final DBI jdbi = factory.build(environment, config.getDataSourceFactory(), "mysql");
      final MyDao dao = jdbi.onDemand(MyDao.class);
        environment.servlets().setSessionHandler(new SessionHandler());
      environment.jersey().register(new ExampleResource(dao));
    }


}

