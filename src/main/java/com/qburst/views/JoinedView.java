package com.qburst.views;

import io.dropwizard.views.View;

/**
 * Created by nithin on 27/4/15.
 */
public class JoinedView extends View {
    private final String name;
    private final String message;

    public JoinedView(String message){
        super("joined.ftl");
        this.name="";
        this.message=message;
    }
    public JoinedView(String name,String message){
        super("joined.ftl");
        this.name=name;
        this.message=message;
    }

    public String getname() {
        return name;
    }

    public String getMessage() {
        return message;
    }
}
