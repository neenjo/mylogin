package com.qburst.dao;

import com.qburst.core.UserData;
import com.qburst.modals.UserMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

/**
 * Created by nithin on 23/4/15.
 */
@RegisterMapper(UserMapper.class)
public interface MyDao {

    @SqlQuery("select username from user where username = :username and password = :password")
    List<UserData> loginValidate(@Bind("username") String username,@Bind("password") String password);

    @SqlQuery("select username from user where username = :username")
    List<UserData> findDuplicate(@Bind("username") String username);

    @SqlUpdate("insert into user (name,username,password)values (:name, :username, :password)")
    void addUser(@BindBean UserData newuser);

}
