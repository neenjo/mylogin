package com.qburst.modals;

/**
 * Created by nithin on 27/4/15.
 */

        import com.qburst.core.UserData;
        import org.skife.jdbi.v2.StatementContext;
        import org.skife.jdbi.v2.tweak.ResultSetMapper;

        import java.sql.ResultSet;
        import java.sql.SQLException;

public class UserMapper implements ResultSetMapper<UserData> {
    public UserData map(int index, ResultSet resultSet, StatementContext statementContext) {
        UserData user = new UserData();
        try {
            user.setName(resultSet.getString("name"));
            user.setUsername(resultSet.getString("username"));
            user.setPassword(resultSet.getString("password"));
        } catch (SQLException e) {}
        return user;
    }
}