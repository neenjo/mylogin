package com.qburst.resource;

import com.qburst.core.UserData;
import com.qburst.dao.MyDao;
import com.qburst.utils.HashUtils;
import com.qburst.utils.RoutingUtils;
import com.qburst.views.JoinedView;
import com.qburst.views.LoginView;
import com.qburst.views.signUpView;
import io.dropwizard.jersey.sessions.Session;
import io.dropwizard.views.View;

import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by nithin on 23/4/15.
 */
@Path("/")
public class ExampleResource {
    private MyDao dao;
    private String name=" ";
    private String message="Welcome you are now logged in";

    public ExampleResource(MyDao dao) {
        this.dao = dao;
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public JoinedView getUser(@Session HttpSession session) {
        RoutingUtils.authenticate(session);
        return new JoinedView(((String) session.getAttribute("username"))," Welcome");
    }

    @POST
    @Produces(MediaType.TEXT_HTML)
    public JoinedView logouter(@Session HttpSession session) {
        session.setAttribute("username", "");
        System.out.println("out");
        session.invalidate();
        RoutingUtils.redirectToURI("/login");
        return new JoinedView("");
    }




    @Path("/login")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public LoginView login() {
        return new LoginView();
    }

    @Path("/login")
    @POST
    @Produces(MediaType.TEXT_HTML)
    public View newFetch(@FormParam("username") String username,@FormParam("password") String password, @Session HttpSession session) {
        String hash = HashUtils.generateHash(password);
        List<UserData> listDuplicate = dao.loginValidate(username,hash);
        if (!listDuplicate.isEmpty()) {
            System.out.println("Logged in");
            session.setAttribute("username", username);
            RoutingUtils.redirectToURI("/");
        }
            System.out.println("Incorrect");

            return new LoginView("The username or password you entered is incorrect");


    }




    @Path("/joined")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public JoinedView join() {
        return new JoinedView(name,message);
    }

    @Path("/joined")
    @POST
    @Produces(MediaType.TEXT_HTML)
    public JoinedView logout(@Session HttpSession session) {
        session.setAttribute("username", "");
        System.out.println("out");
        session.invalidate();
        RoutingUtils.redirectToURI("/login");
        return new JoinedView("");
    }

    @Path("/signup")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public signUpView getsignUpView() {
        return new signUpView();
    }
        @Path("/signup")
        @POST
        @Produces(MediaType.TEXT_HTML)
        public View fetch(@FormParam("name") String name,@FormParam("username") String username,@FormParam("password") String password,@FormParam("confirmPassword") String confirmPassword){
            List<UserData> listDuplicate = dao.findDuplicate(username);
            Pattern p = Pattern.compile("[^a-z0-9_ ]", Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(username);
            boolean b = m.find();

            if (!password.equals(confirmPassword)) {
                return new signUpView(username," your password and confirmation password do not match. [******] - has been ignored.");
            }
            else if (!listDuplicate.isEmpty()) {
                username="";
                return new signUpView(username, "Username already in Use");
            }
            else if(username.trim().length()==0||password.trim().length()==0||name.trim().length()==0)
            {
                username="";
                return new signUpView(username, "Can't Accept Empty Fields");
            }
            else if(username.length()>=40||password.length()>=40||name.length()>=40)
            {
                username="";
                return  new signUpView(username,"Field is too long -data ignored");
            }
            else if(b||username.contains(" ")){
                username="";
                return  new signUpView(username,"Please use only letters (a-z), numbers, and periods.");

            }
            else if(password.length()<=6)
            {
                return  new signUpView(" "," Password: Too short ");

            }
            else {
                String hash=HashUtils.generateHash(password);
                final UserData newUser = new UserData(name,username,hash);
                this.name = name;
                dao.addUser(newUser);
                RoutingUtils.redirectToURI("/joined");
                return new JoinedView(username, "Welcome, you are now logged in");
            }

        }


    }
